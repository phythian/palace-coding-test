# frozen_string_literal: true

require_relative 'product/special_price'

module PalaceCodingTest
  class Product
    attr_reader :sku, :unit_price, :special_price
    private :special_price

    def self.to_proc
      ->(item) { new(item) }
    end

    def initialize(sku:, unit_price:, special_price: nil)
      @sku = sku
      @unit_price = unit_price
      @special_price = special_price
    end

    def discount_rules?
      !special_price.to_h.empty?
    end

    def special_price_rules
      @special_price_rules ||= SpecialPrice.new(special_price) if discount_rules?
    end
  end
end
