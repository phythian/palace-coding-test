# frozen_string_literal: true

module PalaceCodingTest
  class Product
    class SpecialPrice
      attr_reader :discounted_price, :quantity_required

      def initialize(discounted_price:, quantity_required:)
        @discounted_price = discounted_price
        @quantity_required = quantity_required
      end
    end
  end
end
