# frozen_string_literal: true

module PalaceCodingTest
  class DiscountCalculator
    MismatchedItemsError = Class.new(StandardError)

    attr_reader :products
    private :products

    def self.to_proc
      ->(products) { new(products) }
    end

    def initialize(products)
      @products = products
    end

    def total_price
      raise MismatchedItemsError if products.uniq(&:sku).size > 1

      if discount_rules?
        unit_price_total + discount_price_total
      else
        no_discount_total
      end
    end

    private

    def discount_rules?
      products.first.discount_rules?
    end

    def no_discount_total
      number_of_products * normal_price
    end

    def discount_price_total
      number_of_discounts * discount_price
    end

    def unit_price_total
      number_of_normally_priced * normal_price
    end

    def number_of_discounts
      number_of_products - number_of_normally_priced
    end

    def number_of_normally_priced
      number_of_products % discount_quantity
    end

    def normal_price
      products.first.unit_price
    end

    def discount_price
      products.first.special_price_rules.discounted_price
    end

    def discount_quantity
      products.first.special_price_rules.quantity_required
    end

    def number_of_products
      products.size
    end
  end
end
