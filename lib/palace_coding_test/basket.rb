# frozen_string_literal: true

require_relative 'product'
require_relative 'discount_calculator'

module PalaceCodingTest
  class Basket
    InvalidStockError = Class.new(StandardError)

    attr_reader :pricing_rules, :items
    private :pricing_rules

    def initialize(pricing_rules)
      @pricing_rules = pricing_rules
      @items = []
    end

    def total
      discounted_prices.sum(&:total_price)
    end

    def add(item)
      unless stock_keeping_units.include?(item.upcase)
        raise InvalidStockError, invalid_stock_error_message
      end

      items << item.upcase
    end

    private

    def invalid_stock_error_message
      "Expected one of the following SKUs: #{stock_keeping_units.join(', ')}"
    end

    def discounted_prices
      grouped_products.map(&DiscountCalculator)
    end

    def grouped_products
      products.group_by(&:sku).values
    end

    def stock_keeping_units
      product_rules.map(&:sku)
    end

    def products
      items.map do |item|
        product_rules.detect { |product| product.sku == item }
      end
    end

    def product_rules
      @product_rules ||= pricing_rules.map(&Product)
    end
  end
end
