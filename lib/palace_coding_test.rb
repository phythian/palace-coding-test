# frozen_string_literal: true

require 'palace_coding_test/version'
require 'palace_coding_test/basket'

module PalaceCodingTest
  class Error < StandardError; end
end
