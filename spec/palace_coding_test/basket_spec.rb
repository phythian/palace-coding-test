# frozen_string_literal: true

module PalaceCodingTest
  RSpec.describe Basket do
    subject { described_class.new(pricing_rules) }

    let(:pricing_rules) do
      [
        {
          sku: sku,
          unit_price: unit_price,
          special_price: special_price
        }
      ]
    end
    let(:sku) { 'A' }
    let(:unit_price) { 10 }
    let(:special_price) { { discounted_price: 8, quantity_required: 2 } }

    describe '#total' do
      context 'a simple example of only type of item' do
        before do
          3.times { subject.add('A') }
        end

        it 'calculates the total price, taking into account the special pricing' do
          expect(subject.total).to eql(26)
        end
      end

      context 'three different products' do
        let(:pricing_rules) do
          [
            {
              sku: 'A',
              unit_price: 10,
              special_price: { discounted_price: 8, quantity_required: 2 }
            },
            {
              sku: 'B',
              unit_price: 15,
              special_price: { discounted_price: 11, quantity_required: 3 }
            },
            {
              sku: 'C',
              unit_price: 7,
              special_price: { discounted_price: 4, quantity_required: 5 }
            }
          ]
        end

        context 'no special quantities reached' do
          before do
            subject.add('A')
            subject.add('B')
            subject.add('C')
          end

          it 'sums up their unit prices' do
            expect(subject.total).to eql(32)
          end
        end

        context 'some special quantities reached' do
          before do
            3.times { subject.add('A') }
            3.times { subject.add('B') }
            3.times { subject.add('C') }
          end

          it 'calculates the price based on the rules' do
            expect(subject.total).to eql(80)
          end
        end
      end
    end

    describe '#add' do
      context 'the item is valid' do
        let(:item) { 'A' }
        let(:items) { [item] }

        it 'adds the item to an array of items' do
          expect(subject.add(item)).to eql(items)
        end
      end

      context 'the item is invalid' do
        let(:item) { 'Z' }

        it 'raises an invalid stock error' do
          expect { subject.add(item) }
            .to raise_error(
              Basket::InvalidStockError,
              'Expected one of the following SKUs: A'
            )
        end
      end
    end
  end
end
