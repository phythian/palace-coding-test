# frozen_string_literal: true

module PalaceCodingTest
  class Product
    RSpec.describe SpecialPrice do
      subject { described_class.new(rule) }

      let(:rule) do
        {
          discounted_price: discounted_price,
          quantity_required: quantity_required
        }
      end
      let(:discounted_price) { 8 }
      let(:quantity_required) { 2 }

      describe '#discounted_price' do
        it 'returns the discounted price' do
          expect(subject.discounted_price).to eql(discounted_price)
        end
      end

      describe '#quantity_required' do
        it 'returns the quantity required to acquire the discount' do
          expect(subject.quantity_required).to eql(quantity_required)
        end
      end
    end
  end
end
