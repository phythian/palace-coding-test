# frozen_string_literal: true

module PalaceCodingTest
  RSpec.describe DiscountCalculator do
    subject { described_class.new(products) }

    let(:products) { Array.new(3, product) }
    let(:product) do
      double(
        'Product',
        sku: 'A',
        unit_price: 10,
        special_price_rules: special_price,
        discount_rules?: discount_rules
      )
    end
    let(:special_price) do
      double(
        'SpecialPrice',
        discounted_price: 8,
        quantity_required: 2
      )
    end
    let(:discount_rules) { true }
    let(:expected_total_price) { 26 }

    describe '#total_price' do
      context 'there are discounts for multiple purchases' do
        it 'calculates the total price based on the special price rules' do
          expect(subject.total_price).to eql(expected_total_price)
        end
      end

      context 'there are no discounts for multiple purchases' do
        let(:special_price) { nil }
        let(:discount_rules) { false }
        let(:expected_total_price) { 30 }

        it 'calculates the total price' do
          expect(subject.total_price).to eql(expected_total_price)
        end
      end

      context 'there are more than 1 type of product' do
        let(:products) { [product, different_product] }
        let(:different_product) do
          double(
            'Product',
            sku: 'B',
            unit_price: 20,
            special_price: special_price
          )
        end

        it 'raises a MismatchedItemsError' do
          expect { subject.total_price }
            .to raise_error(DiscountCalculator::MismatchedItemsError)
        end
      end
    end
  end
end
