# frozen_string_literal: true

module PalaceCodingTest
  RSpec.describe Product do
    subject { described_class.new(item) }

    let(:item) do
      {
        sku: sku,
        unit_price: unit_price,
        special_price: special_price
      }
    end
    let(:sku) { 'A' }
    let(:unit_price) { 10 }
    let(:special_price) { { discounted_price: 8, quantity_required: 2 } }

    describe '.to_proc' do
      subject { described_class.to_proc.call(item) }

      it { is_expected.to be_an(described_class) }
    end

    describe '#discount_rules?' do
      subject { described_class.new(item).discount_rules? }

      context 'there are special pricing rules' do
        it { is_expected.to be true }
      end

      context 'an empty hash is provided' do
        let(:special_price) { {} }

        it { is_expected.to be false }
      end

      context 'no parameter is provided' do
        let(:item) do
          {
            sku: sku,
            unit_price: unit_price
          }
        end

        it { is_expected.to be false }
      end
    end

    describe '#special_price_rules' do
      it 'returns the special_price rules from the hash' do
        expect(subject.special_price_rules).to be_an_instance_of(Product::SpecialPrice)
      end
    end
  end
end
