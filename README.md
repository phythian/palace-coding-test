# PalaceCodingTest

## The Challenge

We're going to implement the code for an online basket, that will calculate
the total price of the items added to it. On our website, products are
identified using Stock Keeping Units, or SKUs. In this example, we'll use
individual letters to keep things simple (A, B, C and so on). Our products
are priced individually, but some items are multipriced - if you buy three of
A for example, you'll get a discount.

## The Solution

### Project Initiation

_Skip to Usage to bypass this verbose explanation._
 
I started solving this by creating a `Basket` class that stores added items and
returns the `total` price, a `Product` class that handles the logic for each
of the `pricing_rules`, and a `SpecialPrice` class that holds the `Product`'s
discount logic.

After initiating a `Basket`, it simply stores the added items in an array until
the total needs to be calculated. When the total is calculated, it maps all
the provided SKUs to the `Product`s initiated with the Basket.

Initially ignoring the discount logic, I simply had the `total` as the sum of
all the items' unit prices. Verifying that this worked, it was time to take
the discounts into account.

### Calculating Discount

To calculate the discounts I started by grouping the added products by their
SKU, giving me an array of arrays of identical products.

The simplest way, in my mind, to figure out each product's sub-total was to
find the modulus of the total quantity relative to the quantity required for a
discount.

For example, if you have 7 'A' products, and there is a "3 for _x_" deal, then
`7 % 3` will return the number of products _not_ qualifying for a discount -
in this case, 1. All remaining products will qualify for a discount - in this
case, 6.

To find the sub-total you would sum 6 of the discount prices with 1 of the
unit price. Easy peasy!

I created a `DiscountCalculator` class to do this. Provide it an array of
`Products`, it will verify their validity (i.e. only one SKU), and then perform
the above logic.

Self-admittedly I initially forgot that a Product could have _no_ discount.
This meant I had to go back and change some logic. You cannot divide by 0 so
the above would not work as simply as I'd hoped. By giving the `Product` class
a `discount_rules?` instance method I could check whether there are any rules,
and if not, simply multiply the unit price by the quantity.

So to find the grand total:

- group the added products by their SKU

- map this array of arrays through the `DiscountCalculator`

- find the `.sum(&:total_price)` of this array.

- Voila!

### Niceties

To make this gem nice to use I added various tidbits:

- no SKU case-sensitivity

- a helpful error message when an invalid SKU is provided, showing valid SKUs

- the Basket is slightly flexible when handling the `special_price` parameter:

    - Empty hash? No discount.

    - parameter is `nil`? No discount.

    - parameter is missing? No discount.

- SimpleCov to verify my test coverage is 100%

- Rubocop (with a few config changes) to verify this adheres to Ruby conventions

- `self.to_proc` class methods on most classes to allow prettier mapping:

    - `array.map(&Class)` rather than `array.map { |x| Class.new(x) }`

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'palace_coding_test'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install palace_coding_test

## Usage

The Basket expects a `pricing_rules` (unnamed) parameter - which should be an array of
hashes containing:

- an alphabetised `sku` (not case-sensitive)

- a numeric `unit_price`

- an optional `special_price` hash - containing:

    - the numeric discounted price

    - numeric quantity required to achieve said discount

For example - a unit `A` could cost `50` but has a `3 for 130` discount.

The pricing rules would look as follows:
```
pricing_rules = [
  {
    sku: 'A',
    unit_price: 50,
    special_price: { discounted_price: 130/3.0, quantity_required: 3 }
  }
]
```

A full example `pricing_rules`, where C & D have no discounts:
```
pricing_rules = [
  {
    sku: 'A',
    unit_price: 10,
    special_price: { discounted_price: 8, quantity_required: 2 }
  },
  {
    sku: 'B',
    unit_price: 25,
    special_price: { discounted_price: 21, quantity_required: 3 }
  },
  {
    sku: 'C',
    unit_price: 20,
    special_price: {}
  },
  {
    sku: 'D',
    unit_price: 30,
  }
]
```

As per the requirements you can initiate a `Basket` as follows:
```
basket = PalaceCodingTest::Basket.new(pricing_rules)
```

and add items, with a friendly lack of case-sensitivity:
```
basket.add('A')
basket.add('a')
basket.add('A')
basket.add('B')
basket.add('b')
basket.add('c')
basket.add('D')
```

and find the total:
```
price = basket.total # 126
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/palace_coding_test.

